Small App Boilerplate
================

This boilerplate code is ideal for getting a small mobile app off the ground quickly with Meteor. Includes the Ratchet mobile UI.
(http://goratchet.com/)

Packages:
- Iron Router
- stevezhu:sass (https://atmospherejs.com/stevezhu/sass)

Thanks to nickw for his Meteor Mailbox sample showing how to get page transitions in Ratchet working with Meteor.
https://github.com/nickw/meteor-mailbox